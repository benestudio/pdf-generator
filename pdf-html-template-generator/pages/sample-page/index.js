import React from 'react';
import Page from '../../containers/page';
import styled from 'styled-components';
import CircleChart from '../../components/CircleChart';

const Content = styled.div`
  position: relative;
  padding-top: 70px;
  color: #636663;
  font-size: 27px;
  line-height: 1.3;

  p {
    margin-bottom: 70px;
  }
`;

const ChartWrapper = styled.div`
  display: flex;
  justify-content: center;
`;

const Img = styled.img`
  width: 42px;
  margin-left: 16px;
  position: absolute;
  top: 20px;
  left: 20px;
`;

const IntroPage = props => {
  const circleChartProps = {
    values: [+props.chartValue],
    colors: ['red'],
    subtitle: 'Chart value',
    title: `${props.chartValue}%`,
    innerCircleColor: 'white',
  };
  return (
    <Page {...props}>
      <Content>
        <ChartWrapper>
          <Img src={`${props.host}/images/logo.png`} />
          <CircleChart {...circleChartProps} />
        </ChartWrapper>
      </Content>
    </Page>
  );
};

export default IntroPage;
