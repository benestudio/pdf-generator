import React from 'react';
import styled from 'styled-components';
import moment from 'moment';
import Page from '../../containers/page';

const Content = styled.div`
  position: absolute;
  color: #ffffff;
  top: 0;
  left: 0;
  right: 0;
  bottom: 0;
  padding: 24px;
  background-size: cover;
  background-position: center;
  h1 {
    font-size: 29px;
    text-transform: uppercase;
    margin: 0;
    font-weight: 800;
    margin-top: 20px;
    width: 590px;
  }

  h4 {
    font-size: 27px;
    margin: 0;
    font-weight: 400;
    line-height: 1.3;
  }
  p {
    font-size: 11px;
    position: absolute;
    left: 1.15cm;
    bottom: 30px;
  }
  a {
    color: white;
  }
`;

const Header = styled.div`
  color: white;
  display: flex;
  align-items: center;
  font-size: 17px;
`;

const CoverPage = props => {
  return (
    <Page {...props} frontPage>
      <Content
        style={{
          background: 'red',
        }}
      >
        <Header>PDF GENERATOR NAME</Header>
        <h1>{props.title}</h1>
        <p>{moment().format('MMM. Do, YYYY')}</p>
      </Content>
    </Page>
  );
};

export default CoverPage;
