import React from 'react';
import styled from 'styled-components';
import Page from '../../containers/page';

const Content = styled.div`
  position: absolute;
  color: #ffffff;
  top: 0;
  left: 0;
  right: 0;
  bottom: 0;
  padding: 24px;
  color: #ffffff;
  h1 {
    font-size: 34px;
    text-transform: uppercase;
    margin: 0;
  }

  h4 {
    font-size: 27px;
    margin: 0;
    font-weight: 400;
    line-height: 1.3;
  }
  p {
    font-size: 21px;
  }
  a {
    color: white;
  }
`;

const EndPage = props => {
  return (
    <Page {...props} endPage>
      <Content style={{ backgroundColor: 'red' }}>
        <h1>{props.title}</h1>
        <p>
          <a href="https://benestudio.co/">https://benestudio.co/</a>
        </p>
      </Content>
    </Page>
  );
};

export default EndPage;
