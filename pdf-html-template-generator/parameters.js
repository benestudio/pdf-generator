// https://www.papersizes.org/a-sizes-in-pixels.htm

const sizes = {
  A4: {
    ppi96: {
      width: 1123,
      height: 794,
    },
  },
};

export const pageSize = sizes.A4.ppi96;
