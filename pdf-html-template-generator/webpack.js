const path = require('path');

module.exports = {
  entry: './pdf-html-template-generator/index.js',
  mode: 'production',
  target: 'node',
  output: {
    path: path.resolve(__dirname, '../server/bundles'),
    filename: 'pdf-html-template-generator.bundle.js',
    libraryTarget: 'commonjs-module',
  },
  module: {
    rules: [
      {
        test: /\.m?js$/,
        exclude: /(node_modules|bower_components|app)/,
        use: {
          loader: 'babel-loader',
          options: {
            presets: ['@babel/preset-env'],
          },
        },
      },
    ],
  },
};
