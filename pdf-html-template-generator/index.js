import React, { Fragment } from 'react';
import { renderToString } from 'react-dom/server';
import Html from './containers/html';
import { ServerStyleSheet } from 'styled-components';
import CoverPage from './pages/cover-page';
import EndPage from './pages/end-page';
import SamplePage from './pages/sample-page';
import { pageSize } from './parameters';

export default (data, host) => {
  const sheet = new ServerStyleSheet();
  const body = renderToString(
    sheet.collectStyles(
      <Fragment>
        <CoverPage {...data} host={host} />
        <SamplePage {...data} host={host} />
        <EndPage host={host} title="End page title" />
      </Fragment>,
    ),
  );
  const styles = sheet.getStyleTags();

  return {
    html: Html({
      body,
      styles,
    }),
    pageSize,
  };
};
