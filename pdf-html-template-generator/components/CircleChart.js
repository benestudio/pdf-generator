import React from 'react';
import styled from 'styled-components';

const Subtitle = styled.div`
  text-align: center;
  font-size: 14px;
  font-weight: 500;
  position: absolute;
  width: 100%;
  color: #454545;
  top: 157px;
`;

const Title = styled.div`
  text-align: center;
  font-size: 32px;
  font-weight: 700;
  color: black;
  position: absolute;
  width: 100%;
  top: 118px;
  font-family: 'PT Mono', monospace;
  div {
    display: inline-block;
    margin-left: 3px;
    font-size: 13px;
    color: #454545;
    font-family: 'Montserrat', sans-serif;
  }
`;

const DiagramWrapper = styled.div`
  position: relative;
  display: block;
  svg {
    display: block;
  }
`;

const getAngle = val => {
  return (-val / 100) * Math.PI * 2 + Math.PI * 0.5;
};

const getAnglePosition = (values, index) => {
  let angle = 0;
  for (let i = 0; i < index; i++) {
    angle += values[i];
  }
  return angle;
};

const CircleChart = ({ values, colors, subtitle, title, r = 150, innerCircleColor = 'white' }) => {
  const defaultArc = {
    x: r,
    y: r,
    r: r,
  };
  return (
    <DiagramWrapper>
      <svg width={r * 2} height={r * 2}>
        <circle cx={r} cy={r} r={r} fill="white" />
        {values.map((value, index) => (
          <path
            key={index}
            d={createSvgArc({
              ...defaultArc,
              startAngle: getAngle(getAnglePosition(values, index)),
              endAngle: getAngle(getAnglePosition(values, index + 1)),
            })}
            fill={colors[index]}
          />
        ))}
        <circle cx={r} cy={r} r={r * 0.7} fill={innerCircleColor} stroke="#dddddd" />
      </svg>
      {title && <Title>{title}</Title>}
      {subtitle && <Subtitle>{subtitle}</Subtitle>}
    </DiagramWrapper>
  );
};

function createSvgArc({ x, y, r, startAngle, endAngle }) {
  if (startAngle > endAngle) {
    var s = startAngle;
    startAngle = endAngle;
    endAngle = s;
  }
  if (endAngle - startAngle > Math.PI * 2) {
    endAngle = Math.PI * 1.99999;
  }

  var largeArc = endAngle - startAngle <= Math.PI ? 0 : 1;

  return [
    'M',
    x,
    y,
    'L',
    x + Math.cos(startAngle) * r,
    y - Math.sin(startAngle) * r,
    'A',
    r,
    r,
    0,
    largeArc,
    0,
    x + Math.cos(endAngle) * r,
    y - Math.sin(endAngle) * r,
    'L',
    x,
    y,
  ].join(' ');
}

export default CircleChart;
