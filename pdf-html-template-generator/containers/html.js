const Html = ({ body, styles }) => `
  <!DOCTYPE html>
  <html>
    <head>
      <title> PDF </title>
      <link href="https://fonts.googleapis.com/css?family=Montserrat:500,600&display=swap" rel="stylesheet">
      <link href="https://fonts.googleapis.com/css?family=Lora:400&display=swap" rel="stylesheet">
      ${styles}
      
      <style>
        body {
          font-family: 'Montserrat', sans-serif;
          background-color: grey;
          margin: 0;
        }
        * {
          -webkit-print-color-adjust: exact;
        }
      </style>
    </head>
    <body>
      <div id="app"> ${body} </div>
    </body>
  </html>
`;

export default Html;
