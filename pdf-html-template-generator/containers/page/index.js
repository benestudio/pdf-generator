import React from 'react';
import styled from 'styled-components';
import Footer from './footer';
import { pageSize } from '../../parameters';

const StyledPage = styled.div`
  background: #ffffff;
  width: ${pageSize.width}px;
  height: ${pageSize.height}px;
  display: flex;
  align-items: center;
`;

const Screen = styled.div`
  background: #ffffff;
  width: 100%;
  height: 100%;
  position: relative;
`;

const Page = props => (
  <StyledPage>
    <Screen>
      {props.children}
      <Footer {...props} />
    </Screen>
  </StyledPage>
);

export default Page;
