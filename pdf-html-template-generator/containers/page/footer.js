import React from 'react';
import styled from 'styled-components';

const StyledFooter = styled.div`
  position: absolute;
  bottom: 24px;
  right: 24px;
  left: 24px;

  p {
    margin-bottom: 16px;
    font-size: 15px;
    color: #999999;
    a {
      color: inherit;
    }
  }
`;

const Footer = ({ endPage, host, frontPage }) => {
  if (frontPage) return null;
  return (
    <StyledFooter>
      <p style={{ color: endPage ? 'white' : '' }}>
        Generated with Bene : studio from <a href="https://benestudio.co">https://benestudio.co/</a>
      </p>
    </StyledFooter>
  );
};

export default Footer;
