import express from 'express';
import path from 'path';
import rawData from './rawData';
import index from './index';

const port = 4000;
const server = express();

server.use(express.static(path.resolve(__dirname, 'public')));
server.get('/', (req, res) => {
  const doc = index(rawData, `http://localhost:${port}`);
  res.send(doc.html);
});

server.listen(port);
console.log(`Serving PDF HTML template generator in Development mode at http://localhost:${port}`);
