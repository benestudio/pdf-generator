# PDF generator on the server with React

Use npm run dev:pdf to develop the generator.
Use npm run build:pdf to build a bundle.

To copy static file form public folder to servers public folder, use Webpack copy plugin or copy it yourself (not recommanded if you develop it regularly).

From the server and client part use only the createPdfFromTemplate method and use your own code for the server.

Can be refactored and use the same components in the generator like in the client React app itself, but be carefull with the sizes.
