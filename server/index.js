import express from 'express';
import path from 'path';
import bodyParser from 'body-parser';

/* USE THIS PART - START */
import { default as bundle } from './bundles/pdf-html-template-generator.bundle.js';
import createPdfFromTemplate from './createPdfFromTemplate';
/* USE THIS PART - END */

const port = 3000;
const server = express();

server.set('view engine', 'ejs');

server.use(
  bodyParser.urlencoded({
    extended: true,
    limit: '10mb',
  }),
);

server.use(
  bodyParser.json({
    limit: '10mb',
  }),
);

server.get('/', (req, res) => {
  res.render(path.resolve(__dirname, '../client/index'));
});

server.use(express.static(path.resolve(__dirname, 'public')));

server.post('/generate-pdf', async (req, res) => {
  const { body } = req;
  /* USE THIS PART - START */
  const doc = bundle(body, 'http://localhost:3000');
  const blob = await createPdfFromTemplate(doc);
  res.header('Content-Type', 'application/pdf');
  res.send(blob);
  /* USE THIS PART - END */
});

server.listen(port);
