import puppeteer from 'puppeteer';

const totalFonts = 2; // TODO from a config file
async function fonts(page) {
  let fontsLoaded = 0;
  return new Promise(resolve => {
    page.on('response', response => {
      if (response._request._headers['sec-fetch-dest'] === 'font') {
        // works for google fonts
        fontsLoaded++;
        if (fontsLoaded === totalFonts) {
          resolve();
        }
      }
    });
  });
}

const createPdfFromTemplate = async ({ html, pageSize: { width, height } }) => {
  const browser = await puppeteer.launch({ args: ['--no-sandbox', '--disable-setuid-sandbox'] });
  const page = await browser.newPage();
  await page.setContent(html);
  await page.emulateMedia('screen');
  await fonts(page);
  const pdf = await page.pdf({
    width,
    height: height + 1,
  });
  await browser.close();
  return pdf;
};

export default createPdfFromTemplate;
